# Setup Instructions
1. Create an account on [GitLab](https://gitlab.com/)
1. Fork the [reference hand-in repository](https://gitlab.com/jhilliker/handins)
	1. White "fork" button, top right of project area
1. Set the project visibility of your fork to "private".
	1. Settings
	1. General
	1. Visibility, project features, permissions
	1. Project visibility -> Private
1. Add the instructor and TAs as developers to your project
	1. Settings
	1. Members
	1. member ->
		1. ```jhilliker```
		1. (TA user names to follow)
	1. role -> Developer
1. Report your fork's SSH clone URL to the quiz on d2l/brightspace.
	1. Blue "clone" button, top right of project area
	1. "Clone with SSH"
	1. eg: ```git@gitlab.com:jhilliker/handins.git```
1. Generate an ssh keypair and add it to your account
    1. On your machine(s): 
        1. ```ssh-keygen -t ed25519 -o -C "computerName"``` (replacing ```computerName``` with a name for this computer)
    1. Keep default file location
    1. Copy your public key: ```cat ~/.ssh/id_ed25519.pub```
    1. Go to: [your gitlab profile settings](https://gitlab.com/profile)
        1. SSH Keys
        1. Add your key via the text box
    1. Test your keys on your machine: ```ssh -T git@gitlab.com```
        1. Answer "yes" if prompted
1. Set your git name and email
    1. Get your gitlab private email
        1. Go to [your gitlab profile settings page](https://gitlab.com/profile)
        1. Change "Commit email" to "Use a private email"
        1. Click green "Update profile settings" button
        1. Open a javascript console
            1. Firefox: ctrl-shift-k
            1. Chrome: ctrl-shift-j
        1. Copy the email address returned from running the following javascript code:
            1. ```Array.from(document.getElementsByTagName("option")).map(o => o.text).find(t => /^\d+[-+]\S+@users.noreply.git(hub|lab).com$/.test(t))```
            1. OR ```document.getElementById('user_id').value + '-' + document.getElementsByClassName('profile-link')[0].getAttribute('data-user') + '@users.noreply.gitlab.com'```
        1. On your machine:
            1. ```git configure --global user.email "emailAddress"``` (replacing ```emailAddress``` with the email obtained above)
            1. ```git configure --global user.name "aName"``` (replacing ```aName``` with your name [or something...])
1. Clone your fork to your local machine: ```git clone cloneURL``` (replacing ```cloneURL``` with the SSH clone URL obtained earlier)
            

# Assignment instructions

```sh
until isAssignmentDone ; do
	doAnswerQuestions
	git add .   # marks files for commit
	git commit  # commits to local repo
	git push    # pushes commits to server
done
```

# Useful Links

* UNIX
	* [POSIX 2017](https://pubs.opengroup.org/onlinepubs/9699919799/?ou=138568)
	* [linux.die.net - Linux Documentation](https://linux.die.net/)
	* [man7 - Linux man pages](http://man7.org/linux/man-pages/)
	* [GNU Manuals Online](https://www.gnu.org/manual/)
* git
	* [Git Cheat Sheet](https://education.github.com/git-cheat-sheet-education.pdf)
	* [git slide deck](https://drive.google.com/open?id=1NNQ5yDb8F4p0HEecv0CrbUY38T44yLhV)
