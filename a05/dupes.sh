#!/bin/bash

hasDupes(){
cmp -s "$1" "$2"
if [ $? = 1 ]; then
    return 1
else
    return 0
fi
}


if [ $# -eq 0 ]
then
    for file in *
    do
        for filex in *
        do
            if hasDupes $file $filex -eq 1
            then
            filename="$filename$filex:"
            fi
        done
        filename=${filename::-1}
        filename="$filename\n"
    done 
    printf $filename
else
    for file in $1
    do
        for filex in *
        do
            if hasDupes $file $filex -eq 1
            then
            filename="$filename$filex:"
            fi
        done
        filename=${filename::-1}
        filename="$filename\n"
    done 
    printf $filename
fi